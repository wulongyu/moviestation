# coding:utf8
from flask import Flask

app = Flask(__name__)

app.debug = True
'''是否开启调试模式'''

from app.home import home as home_blueprint
from app.admin import admin as admin_blueprint
#导入蓝图对象

app.register_blueprint(home_blueprint)
app.register_blueprint(admin_blueprint, url_prefix="/admin")

'''注册蓝图，区分前后台'''